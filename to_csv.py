# coding=utf-8
import json, time, csv

FOLDER = '/home/d3vpasha/Téléchargements/cities'
GENERATED_FILE = FOLDER + '/department-'
DEMOGRAPHIC_FILE = FOLDER + '/base-pop-historiques-1876-2017.csv'


# I get a list of numbers like this : [1,2,3,4] and the goal is to ensure that nb of habitants are increasing and attribute a score
def get_score(list_nb_habitants):
	score = 0
	for i in range(len(list_nb_habitants)):
		if(i != 0 and list_nb_habitants[i]): # do not compare first element with -1 because it does not exist. second comparison : do not take values that does not exist
			if(int(float(list_nb_habitants[i-1])) > int(float(list_nb_habitants[i]))):
				score += 1
	return score

# Will take the line from the CSV file and transform it into sthg readable by Kibana.
# The result will be multiple line because separated into years
def transform(line, headers):
	list_nb_habitants = []
	final_line = final_line_w_score = ''
	for index, row in enumerate(line):
		if(index > 3):
			list_nb_habitants.append(row.strip('\n'))
			final_line += '{"' + headers[0] + '": ' + line[0] + ', "' + headers[1] + '": ' + line[1] + ', "' + headers[2] + '": ' + line[2] + ', "' + headers[3] + '": "' + line[3] + '", "' + 'année": 01-01-' + headers[index].rstrip('\n') + ', "nombre d\'habitants": ' + row.rstrip('\n') + '}\n'
	score = get_score(list_nb_habitants)
	# we will add the score just before the end of each line for each year
	splitted_final_line = final_line.split('}')
	for i in splitted_final_line[:-1]:
		final_line_w_score += i + ', "score": ' + str(score) + '}'
	final_line_w_score += '\n'
	return final_line_w_score, line[2]

def read_file(filename, nb_lines):
	with open(filename, 'r') as fp:
		line = fp.readline()
		headers = line.split(',')
		cnt = 1
		while cnt < nb_lines:
			splitted_line = fp.readline().split(',')
			city_line, department = transform(splitted_line, headers)
			write_file(department, city_line)
			cnt += 1

# Takes a string content and writes it to a file
def write_file(filename_suffix, content):
	with open(GENERATED_FILE + filename_suffix + '.' + 'json', 'a') as file:
		file.write(content)
		file.close()

def get_nb_lines(filename):
	with open(filename, 'r') as fp:
		reader = csv.reader(fp)
		return len(list(reader))

def main():
	nb_lines = get_nb_lines(DEMOGRAPHIC_FILE)
	time_start = time.time()
	file_content = read_file(DEMOGRAPHIC_FILE, nb_lines)
	time_end = time.time()
	print(str((time_end - time_start)) + ' seconds')

main()